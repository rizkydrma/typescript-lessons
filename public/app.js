import { Payment } from './classes/Payment.js';
import { Invoice } from './classes/Invoice.js';
import { ListTemplate } from './classes/ListTemplate.js';
const form = document.querySelector('.new-item-form');
const type = document.querySelector('#type');
const toFrom = document.querySelector('#toFrom');
const details = document.querySelector('#details');
const amount = document.querySelector('#amount');
const ul = document.querySelector('ul');
const list = new ListTemplate(ul);
form.addEventListener('submit', (e) => {
    e.preventDefault();
    let values;
    values = [toFrom.value, details.value, amount.valueAsNumber];
    let doc;
    if (type.value === 'invoice') {
        doc = new Invoice(...values);
    }
    else {
        doc = new Payment(...values);
    }
    list.render(doc, type.value, 'end');
});
// GENERICS
const addUID = (obj) => {
    let uid = Math.floor(Math.random() * 100);
    return Object.assign(Object.assign({}, obj), { uid });
};
let docOne = addUID({ name: 'rama', age: 22 });
// let docTwo = addUID("hehhe");
// error because must be object an have property name
console.log(docOne.age);
const docThre = {
    uid: 1,
    resourceName: 'rama',
    data: { score: 100, course: 'RN' },
};
console.log(docThre);
const docFour = {
    uid: 2,
    resourceName: 'estarosa',
    data: ['omaewa', 'sideruyo'],
};
console.log(docFour);
var BookType;
(function (BookType) {
    BookType[BookType["BOOK"] = 0] = "BOOK";
    BookType[BookType["AUTHOR"] = 1] = "AUTHOR";
    BookType[BookType["DIRECTOR"] = 2] = "DIRECTOR";
    BookType[BookType["PUBLISHED"] = 3] = "PUBLISHED";
})(BookType || (BookType = {}));
const someDoc = {
    uid: 3,
    name: 'bung fiersa',
    type: BookType.BOOK,
    data: { name: 'rama', age: 22 },
};
console.log(someDoc);
// TUPLES
let tup = ['rama', 22];
tup[0] = 'ken';
