export class Payment {
    constructor(client, detail, amount) {
        this.client = client;
        this.detail = detail;
        this.amount = amount;
    }
    format() {
        return `${this.client} owed ${this.detail} for ${this.amount}`;
    }
}
