import { HasFormatter } from '../interface/HasFormatter';

export class Payment implements HasFormatter {
  constructor(
    readonly client: string,
    private detail: string,
    public amount: number,
  ) {}

  format() {
    return `${this.client} owed ${this.detail} for ${this.amount}`;
  }
}
