import { HasFormatter } from '../interface/HasFormatter';

export class Invoice implements HasFormatter {
  // readonly client: string;
  // private detail: string;
  // public amount: number;

  constructor(
    readonly client: string,
    private detail: string,
    public amount: number,
  ) {}

  format() {
    return `${this.client} owes ${this.detail} for ${this.amount}`;
  }
}
