import { Payment } from './classes/Payment.js';
import { Invoice } from './classes/Invoice.js';
import { HasFormatter } from './interface/HasFormatter';
import { ListTemplate } from './classes/ListTemplate.js';

const form = document.querySelector('.new-item-form') as HTMLFormElement;

const type = document.querySelector('#type') as HTMLSelectElement;
const toFrom = document.querySelector('#toFrom') as HTMLInputElement;
const details = document.querySelector('#details') as HTMLInputElement;
const amount = document.querySelector('#amount') as HTMLInputElement;

const ul = document.querySelector('ul')!;
const list = new ListTemplate(ul);

form.addEventListener('submit', (e: Event) => {
  e.preventDefault();

  let values: [string, string, number];
  values = [toFrom.value, details.value, amount.valueAsNumber];

  let doc: HasFormatter;
  if (type.value === 'invoice') {
    doc = new Invoice(...values);
  } else {
    doc = new Payment(...values);
  }

  list.render(doc, type.value, 'end');
});

// GENERICS
const addUID = <T extends { name: string }>(obj: T) => {
  let uid = Math.floor(Math.random() * 100);
  return { ...obj, uid };
};

let docOne = addUID({ name: 'rama', age: 22 });

// let docTwo = addUID("hehhe");
// error because must be object an have property name

console.log(docOne.age);

// GENERICS IN INTERFACE
interface Resource<T> {
  uid: number;
  resourceName: string;
  data: T;
}

const docThre: Resource<object> = {
  uid: 1,
  resourceName: 'rama',
  data: { score: 100, course: 'RN' },
};

console.log(docThre);

const docFour: Resource<string[]> = {
  uid: 2,
  resourceName: 'estarosa',
  data: ['omaewa', 'sideruyo'],
};

console.log(docFour);

// ENUMS
interface Book<T> {
  uid: number;
  name: string;
  type: BookType;
  data: T;
}

enum BookType {
  BOOK,
  AUTHOR,
  DIRECTOR,
  PUBLISHED,
}

const someDoc: Book<object> = {
  uid: 3,
  name: 'bung fiersa',
  type: BookType.BOOK,
  data: { name: 'rama', age: 22 },
};

console.log(someDoc);

// TUPLES
let tup: [string, number] = ['rama', 22];
tup[0] = 'ken';
